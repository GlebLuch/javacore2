package CompareTwoFiles;

import java.io.IOException;
import java.nio.file.Path;

public class Main {

    public static void main(String[] args) throws IOException {

        Comparator comparator = new Comparator();
        Path path1 = Path.of("TestFile");
        Path path2 = Path.of("FileToCompare");
        System.out.println(comparator.filesCompare(path1, path2));
    }
}
