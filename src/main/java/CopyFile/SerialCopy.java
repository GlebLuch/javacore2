package CopyFile;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

public class SerialCopy  {

    public static void serialCopy (String path1, String path2) throws IOException {

        FileInputStream instream;
        FileOutputStream outstream;

        try {
            File infile = new File(path1);
            File outfile = new File(path2);

            instream = new FileInputStream(infile);
            outstream = new FileOutputStream(outfile);

            byte[] buffer = new byte[1024];

            int length;

            while ((length = instream.read(buffer)) > 0) {
                outstream.write(buffer, 0, length);
            }

            instream.close();
            outstream.close();

        } catch (IOException ioe) {
            ioe.printStackTrace();
        }
    }
}
