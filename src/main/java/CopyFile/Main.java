package CopyFile;

import java.io.IOException;

public class Main {

    public static void main(String[] args) throws IOException {

        String path1 = "TestFile";
        String path2 = "FileToCompare";
        AsyncFileCopy.asyncCopy(path1, path2);
        SyncFileCopy.syncCopy(path1, path2);
        SerialCopy.serialCopy(path1, path2);
        ParallelCopy.parallelCopy(path1, path2);
    }
}
