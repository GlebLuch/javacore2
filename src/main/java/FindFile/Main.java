package FindFile;

import java.io.IOException;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws IOException {

        Finder finder = new Finder();
        Scanner scan = new Scanner(System.in);
        System.out.println("Введите имя файла, или содержимое");
        String name = scan.next();
        System.out.println("Введите путь");
        String path = scan.next();
        finder.findByName(name, path).forEach(System.out::println);
        finder.findByContent(name, path).forEach(System.out::println);
    }
}
