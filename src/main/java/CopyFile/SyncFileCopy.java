package CopyFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.StandardCopyOption;

public class SyncFileCopy {

    public static void syncCopy (String path1, String path2) throws IOException {

        File file1 = new File(path1);
        File file2 = new File(path2);

        Files.copy(Path.of(file1.getPath()), Path.of(file2.getPath()), StandardCopyOption.REPLACE_EXISTING);
    }

}
