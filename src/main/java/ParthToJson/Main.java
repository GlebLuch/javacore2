package ParthToJson;

import org.json.JSONObject;
import org.json.XML;

import java.io.File;
import java.nio.file.Files;

public class Main {

    public static void main(String[] args) throws Exception{

        File xmlFile = new File("pom.xml");
        byte[] b = Files.readAllBytes(xmlFile.toPath());
        String xml = new String(b);
        JSONObject obj = XML.toJSONObject(xml);
        System.out.println(obj);
    }

}
