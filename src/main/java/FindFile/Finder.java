package FindFile;

import java.io.File;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.stream.Stream;

public class Finder {

    public Stream<Path> findByName (String name, String directory) throws IOException {

        return Files.find(Path.of(directory), 100,
                (path, basicFileAttributes) -> {
                    File file = path.toFile();
                    return !file.isDirectory() &&
                            file.getName().equals(name);
                });
    }

    public Stream<Path> findByContent (String content, String directory) throws IOException {
        
           var files = Files.walk(Paths.get(directory));
           return files.filter(f-> {
               try {
                   return Files.readString(f, StandardCharsets.UTF_8).contains(content);
               } catch (IOException e) {
                   e.printStackTrace();
               }
               return false;
           });
    }
}
