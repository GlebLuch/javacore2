package CopyFile;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.Arrays;
import java.util.List;

public class ParallelCopy {

    public static void parallelCopy (String path1, String path2) throws IOException {

        File file1 = new File(path1);
        File file2 = new File(path2);

        List<File> fileList = List.of(file1, file2);

        Arrays.asList(fileList).parallelStream().forEach((filepath) ->{
            Path source = Paths.get(path1);
            Path target = Paths.get(path2);
            try {
                Files.copy(source, target, StandardCopyOption.REPLACE_EXISTING);
            } catch (IOException e) {
                e.printStackTrace();
            }
        });
    }
}
